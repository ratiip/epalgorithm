%inertial Algorithm
%ref : New inertial algorithm for a class of 
%      equilibrium problems 
%BY  : Dang Van Hieu 
%(Numer Algor, 2018.)
clear
%Setting algorithm
mTest = 1;
stopC = 5*1e-3;
max = 1000;
displayInfo = 'The problem size is %d.\n';
displayEpt = ' || x - x* || = %f.\n';
displayIter = 'Final Iteration is %f.\n';
displayTime = 'usedCPUtime %f secs.\n';
error = 'It is run out of maximum iteration.';
options=optimset('MaxFunEvals',100000,'Display','off');

%Choose dimention of the problem
n = 10;

%Choose lambda and theta sequences
lambda = zeros(max,1);
    for i=1:max
        %lambda(i,1)=1/(i+5);
        lambda(i,1)=1/(2*n*i);
    end
theta = zeros(max,1);
    for i=1:max
        %theta(i,1)=0.1;
        theta(i,1)=0.15;
        %theta(i,1)=0.01;
        %theta(i,1)=0.001;
    end
    
%Sate the equilibrium problem
%f: R^n x R^n -> R defined by <Ax,y-x> + || y ||^4 - || x ||^4
B = ones(n);
I = eye(n);
A = B*B.' + (n^2).*I;
nonlcon = @unitdisk; %Choose Closed convex subset of R^n    

%Exact solution
exact = zeros(n,1);

for av=1:mTest
%Choose x_0 and x_1
x0 = ones(n,1);
x1 = ones(n,1);

%collect iteration and error
data(1,2)=norm(x0-exact);
data(1,1)=0;

%Choose initial t_0 for optimization subproblems
t0 = ones(n,1);

%Start inertial algorithm
sTime = cputime;
   for iteration=1:max
       w1 = x1 + theta(iteration,1)*(x1-x0); 
       
       fun1 = @(t) (lambda(iteration,1).*(dot(A*w1,t-w1)+norm(t)^4-norm(w1)^4)) + 0.5*(norm(t-w1)^2);
    
       x0 = x1;
       x1 = fmincon(fun1,t0,[],[],[],[],[],[],nonlcon,options);
            
       data(iteration+1,2)=norm(x1-w1);
       data(iteration+1,1)=iteration;
       
       if iteration == max
          fprintf(error)
       end
       if norm(x1-w1) < stopC
          eTime = cputime -sTime;
          fprintf(displayInfo,n)
          fprintf(displayEpt,norm(x1-w1))
          fprintf(displayIter,iteration)
          fprintf(displayTime,eTime)
       break
       end
       iteration = iteration+1;
  end 
end    
%End inertial algorithm