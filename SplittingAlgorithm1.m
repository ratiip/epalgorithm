%Splitting Algorithm
%ref : A splitting algorithm for equilibrium problem 
%      given by the difference of two bifunctions
%BY  : Pham Ky Anh and Trinh Ngoc Hai 
%(J. Fixed Point Theory Appl., 2018.)
clear
%Setting algorithm
mTest = 1;
stopC = 5*1e-3;
max = 1000;
displayInfo = 'The problem size is %d.\n';
displayEpt = ' || x - x* || = %f.\n';
displayIter = 'Final Iteration is %f.\n';
displayTime = 'usedCPUtime %f secs.\n';
error = 'It is run out of maximum iteration.';
options=optimset('MaxFunEvals',100000,'Display','off');

%Choose dimention of the problem
n = 10;

%Choose lambda sequence
lambda = zeros(max,1);
    for i=1:max
        lambda(i,1)=1/(2*n*i);
    end
    
%Sate the equilibrium problem
%f: R^n x R^n -> R defined by <Ax,y-x> + || y ||^4 - || x ||^4
B = ones(n);
I = eye(n);
A = B*B.' + (n^2).*I;
nonlcon = @unitdisk; %Choose Closed convex subset of R^n    

%Exact solution
exact = zeros(n,1);

for av=1:mTest
%Choose x_0
x0 = ones(n,1);

%collect iteration and error
data(1,2)=norm(x0-exact);
data(1,1)=0;

%Choose initial t_0 for optimization subproblems
t0 = ones(n,1);

%Start Splitting algorithm
sTime = cputime;
   for iteration=1:max

       fun1 = @(t) (lambda(iteration,1).*(dot(A*x0,t-x0))) + 0.5*(norm(t-x0)^2);
    
       y1 = fmincon(fun1,t0,[],[],[],[],[],[],nonlcon,options);
    
       fun2 = @(t) (lambda(iteration,1).*(norm(t)^4-norm(y1)^4)) + 0.5*(norm(t-y1)^2);
       
       x0 = fmincon(fun2,t0,[],[],[],[],[],[],nonlcon,options);
        
       data(iteration+1,2)=norm(x0-exact);
       data(iteration+1,1)=iteration;
       
       if iteration == max
          fprintf(error)
       end
       if norm(x0-exact) < stopC
          eTime = cputime -sTime;
          fprintf(displayInfo,n)
          fprintf(displayEpt,norm(x0-exact))
          fprintf(displayIter,iteration)
          fprintf(displayTime,eTime)
       break
       end
       iteration = iteration+1;
  end 
end    
%End Splitting algorithm